#importing and initial exploration of groundfish Synoptic Bottom Trawl Survey
#Patrick Thompson
#January 2020

#packages#####
library(tidyverse)
library(vegan)
library(cowplot)
library(marmap)
library(RColorBrewer)
library(rgl)
library(viridis)
library(scales) 

#create and set plotting theme####
plt_theme <- theme_bw() + theme(
  plot.background = element_blank(),#element_rect(),    # Background of the entire plot
  
  panel.background = element_rect(),   # Background of plotting area
  panel.border = element_rect(),       # Border around plotting area.
  # fill argument should be NA
  
  panel.grid = element_blank(),         # All grid lines
  panel.grid.major = element_blank(),   # Major grid lines
  panel.grid.minor = element_blank(),   # Minor grid lines
  
  panel.grid.major.x = element_blank(), # Vertical major grid lines
  panel.grid.major.y = element_blank(), # Horizontal major grid lines
  panel.grid.minor.x = element_blank(), # Vertical minor grid lines
  panel.grid.minor.y = element_blank(),  # Vertical major grid lines
  
  strip.background = element_rect(colour=NA, fill=NA),
  strip.text.x = element_text(size = 12),
  strip.text.y = element_text(size = 12),
  axis.title=element_text(size = 12)
)

theme_set(plt_theme)

#import data####
#Straight of Georgia
SOG_biomass <- read.csv("./data/SOG-biomass-pac-dfo-mpo-science-eng.csv")
SOG_biology <- read.csv("./data/SOG-biology-pac-dfo-mpo-science-eng.csv")
SOG_catch <- read.csv("./data/SOG-catch-pac-dfo-mpo-science-eng.csv")
SOG_effort <- read.csv("./data/SOG-effort-pac-dfo-mpo-science-eng.csv")

#West Coast Vancouver Island
WCVI_biomass <- read.csv("./data/WCVI-biology-pac-dfo-mpo-science-eng.csv")
WCVI_biology <- read.csv("./data/WCVI-biology-pac-dfo-mpo-science-eng.csv")
WCVI_catch <- read.csv("./data/WCVI-catch-pac-dfo-mpo-science-eng.csv")
WCVI_effort <- read.csv("./data/WCVI-effort-pac-dfo-mpo-science-eng.csv")

#Hecate Straight
HS_biomass <- read.csv("./data/HS-biology-pac-dfo-mpo-science-eng.csv")
HS_biology <- read.csv("./data/HS-biology-pac-dfo-mpo-science-eng.csv")
HS_catch <- read.csv("./data/HS-catch-pac-dfo-mpo-science-eng.csv")
HS_effort <- read.csv("./data/HS-effort-pac-dfo-mpo-science-eng.csv")

#Queen Charlotte Sound
QCS_biomass <- read.csv("./data/QCS-biology-pac-dfo-mpo-science-eng.csv")
QCS_biology <- read.csv("./data/QCS-biology-pac-dfo-mpo-science-eng.csv")
QCS_catch <- read.csv("./data/QCS-catch-pac-dfo-mpo-science-eng.csv")
QCS_effort <- read.csv("./data/QCS-effort-pac-dfo-mpo-science-eng.csv")

#West Coast Haida Gwaii
WCHG_biomass <- read.csv("./data/WCHG-biomass-pac-dfo-mpo-science-eng.csv")
WCHG_biology <- read.csv("./data/WCHG-biology-pac-dfo-mpo-science-eng.csv")
WCHG_catch <- read.csv("./data/WCHG-catch-pac-dfo-mpo-science-eng.csv")
WCHG_effort <- read.csv("./data/WCHG-effort-pac-dfo-mpo-science-eng.csv")

#combine_data####
effort <- bind_rows(SOG_effort, WCVI_effort, HS_effort, QCS_effort, WCHG_effort)
catch <- bind_rows(SOG_catch, WCVI_catch, HS_catch, QCS_catch, WCHG_catch)

catch_effort <- left_join(effort, catch)

#calculate catch per hour
catch_effort <- catch_effort %>% 
  mutate(Biomass_hr = 60*(Catch.weight..kg./Tow.duration..min.))

catch_effort <- catch_effort %>% 
  filter(!is.na(Catch.weight..kg.)) %>% 
  select(Survey.Year, Trip.identifier, Set.number, Set.date, Start.latitude, Start.longitude, Bottom.depth..m., Scientific.name, Biomass_hr) %>%
  group_by(Survey.Year, Trip.identifier, Set.number, Set.date, Start.latitude, Start.longitude, Bottom.depth..m., Scientific.name) %>% #aggregate all biomass within a single species
  summarise(Biomass_hr = sum(Biomass_hr))

#get bathy map####
bathy.data <- getNOAA.bathy(-135,-123, 48 , 54.75, 
                            resolution = 1, keep=TRUE, antimeridian=FALSE)
bathy.df <- fortify.bathy(bathy.data)
names(bathy.df) <- c("Start.longitude", "Start.latitude", "Bottom.depth..m.")

neg_log_trans = function() trans_new("neg_log", function(x) -log10(-x), function(x) -10^-x) #define new scale for negative log spaced topo lines

bathy.map <- ggplot(bathy.df, aes(x = Start.longitude, y = Start.latitude, z = Bottom.depth..m.))+
  geom_hline(yintercept = seq(48,54,by =2), color = "grey80", size = 0.1)+
  geom_vline(xintercept = seq(-135,-123,by = 3), color = "grey80", size = 0.1)+
  geom_contour(breaks = -exp(seq(log(100),log(3000), length = 15)), color = "grey60", size = 0.1)+
  stat_contour(breaks = 35, colour = "grey30")+
  coord_map(projection = "albers", lat0=min(catch_effort$Start.latitude), lat1=max(catch_effort$Start.latitude), xlim = c(-135.1,-122.9))+
  ylab(NULL)+
  xlab(NULL)+
  theme(legend.position = c(0.85, 0.75))

bathy.map

bathy.map.color <- ggplot(bathy.df, aes(x = Start.longitude, y = Start.latitude, z = Bottom.depth..m.))+
  geom_contour(breaks = -exp(seq(log(100),log(3000), length = 15)), aes(colour = stat(level)), size = 0.1)+
  scale_color_gradient(high = "lightskyblue1", low = "midnightblue", trans = "neg_log", breaks = c(-100,-300,-1000,-3000), guide = FALSE)+
  stat_contour(breaks = 35, colour = "grey30")+
  coord_map(projection = "albers", lat0=min(catch_effort$Start.latitude), lat1=max(catch_effort$Start.latitude))+
  ylab(NULL)+
  xlab(NULL)+
  theme(legend.position = c(0.85, 0.75))

bathy.map.color

bathy.map.cart <- ggplot(bathy.df, aes(x = Start.longitude, y = Start.latitude, z = Bottom.depth..m.))+
  geom_contour(breaks = -exp(seq(log(100),log(3000), length = 15)), color = "grey60", size = 0.1)+
  stat_contour(breaks = 35, colour = "grey30")+
  coord_cartesian()+
  ylab(NULL)+
  xlab(NULL)+
  theme(legend.position = c(0.85, 0.75))

bathy.map.cart

#initial exploration####
bathy.map+
  geom_point(data = catch_effort, aes(color = factor(Survey.Year)), size = 0.5)+
  scale_color_viridis_d()

bathy.map.cart+ 
  geom_hex(data = catch_effort, bins = 80)+
  scale_fill_viridis_c(name = "samples")+
  geom_contour(data = bathy.df, aes(z = Bottom.depth..m.), breaks = -exp(seq(log(100),log(3000), length = 15)), size = 0.1, color = "grey80")
ggsave("./figures/gf_sample_map.pdf", height = 6, width = 6)

total.biomass.df <- catch_effort %>% 
  group_by(Survey.Year, Trip.identifier, Set.number, Set.date, Start.latitude, Start.longitude, Bottom.depth..m.) %>% 
  summarise(Total.biomass.kg.hr.hr = sum(Biomass_hr))

ggplot(total.biomass.df, aes(x = Bottom.depth..m., y = Total.biomass.kg.hr))+
  geom_point()+
  scale_y_log10()+
  scale_x_log10()

#make community matrix####
catch_effort_wide<- catch_effort %>% 
  spread(key = Scientific.name, value = Biomass_hr, fill = 0) %>% 
  ungroup()

catch_effort <- ungroup(catch_effort)

com_matrix <- catch_effort_wide %>% select(ACANTHASTERIDAE:ZOROASTERIDAE) #community matrix
com_matrix_stand <- decostand(com_matrix, method = "total") #community matrix of relative abundances
bray_dist <- vegdist(com_matrix_stand, method = "bray") #Bray-Curtis dissimilarity matrix

#biodiversity metrics####
div_metrics <- renyi(com_matrix, scales = c(0,1,2),hill = TRUE)
diversity.df <- catch_effort_wide %>% select(Survey.Year:Bottom.depth..m.) %>% bind_cols(div_metrics)

plot_grid(
  bathy.map+
    geom_point(data = total.biomass.df, aes(color = Total.biomass.kg.hr), size = 0.5)+
    scale_color_viridis_c(trans = "log10")+
    geom_contour(data = bathy.df, aes(z = Bottom.depth..m.), breaks = -exp(seq(log(100),log(3000), length = 15)), size = 0.1, color = "grey80"),
  
  
  bathy.map+
    geom_point(data = diversity.df, aes(color = `0`), size = 0.5)+
    scale_color_viridis_c(name = "species\nrichness")+
    geom_contour(data = bathy.df, aes(z = Bottom.depth..m.), breaks = -exp(seq(log(100),log(3000), length = 15)), size = 0.1, color = "grey80")
  ,
  
  bathy.map+
    geom_point(data = diversity.df, aes(color = `1`), size = 0.5)+
    scale_color_viridis_c(name = "Shannon\ndiversity")+
    geom_contour(data = bathy.df, aes(z = Bottom.depth..m.), breaks = -exp(seq(log(100),log(3000), length = 15)), size = 0.1, color = "grey80")
  ,
  
  bathy.map+
    geom_point(data = diversity.df, aes(color = `2`), size = 0.5)+
    scale_color_viridis_c(name = "Simpson\ndiversity")+
    geom_contour(data = bathy.df, aes(z = Bottom.depth..m.), breaks = -exp(seq(log(100),log(3000), length = 15)), size = 0.1, color = "grey80")
  , nrow = 2, labels = "AUTO")
ggsave("./figures/gf_diversity.pdf", height = 12, width = 12)

#NMDS analysis####
#nmds <- metaMDS(bray_dist, k = 3)
#nmds_scores <- data.frame(scores(nmds))
#diversity.df<- diversity.df %>% bind_cols(nmds_scores)
#save(diversity.df, file = "./data/diversity.RData")
load("./data/diversity.RData")

diversity.df <- left_join(diversity.df, total.biomass.df)

plot_grid(
  bathy.map+
    geom_point(data = diversity.df, aes(color = NMDS1), size = 0.5)+
    scale_color_viridis_c(name = "NMDS1", direction = -1)+
    geom_contour(data = bathy.df, aes(z = Bottom.depth..m.), breaks = -exp(seq(log(100),log(3000), length = 15)), size = 0.1, color = "grey80"),
  
  bathy.map+
    geom_point(data = diversity.df, aes(color = NMDS2), size = 0.5)+
    scale_color_viridis_c(name = "NMDS2", direction = -1)+
    geom_contour(data = bathy.df, aes(z = Bottom.depth..m.), breaks = -exp(seq(log(100),log(3000), length = 15)), size = 0.1, color = "grey80"),
  
  bathy.map+
    geom_point(data = diversity.df, aes(color = NMDS3), size = 0.5)+
    scale_color_viridis_c(name = "NMDS3", direction = -1)+
    geom_contour(data = bathy.df, aes(z = Bottom.depth..m.), breaks = -exp(seq(log(100),log(3000), length = 15)), size = 0.1, color = "grey80"),
  nrow = 2, labels = "AUTO")
ggsave("./figures/NMDS_map.pdf", height = 12, width = 12)

#cluster analysis####
clusters <- hclust(d = bray_dist, method = "ward.D2")

n.clust <- 5
diversity.df$cluster <- cutree(tree = clusters, k = n.clust)

clust_col <- viridis(n = n.clust, end = 0.95)#brewer.pal(n = n.clust, name = "Set1")

diversity.df <- diversity.df %>% 
  ungroup() %>% 
  group_by(cluster) %>% 
  mutate(Cluster.depth.median = median(Bottom.depth..m., na.rm = TRUE))

diversity.df %>% 
  summarise(number = n(), Cluster.depth.median = mean(Cluster.depth.median)) %>% 
  arrange(desc(Cluster.depth.median))

diversity.df$cluster <- factor(diversity.df$cluster, levels = unique(diversity.df$cluster[order(diversity.df$Cluster.depth.median, decreasing = TRUE)]), ordered = TRUE)

pdf(file = "./figures/cluster_dend.pdf", width = 6, height =5) #had to do the order by hand - will fix later
plot(clusters, labels = FALSE)
rect.hclust(clusters, k = n.clust, border = rev(clust_col)[c(3,5,4,2,1)], cluster = cutree(tree = clusters, k = n.clust))
dev.off()

plot_grid(
  ggplot(diversity.df, aes(x = NMDS1, y = NMDS2, color = factor(cluster)))+
    geom_point()+
    scale_color_manual(values = clust_col, guide = FALSE),
  
  ggplot(diversity.df, aes(x = NMDS1, y = NMDS3, color = factor(cluster)))+
    geom_point()+
    scale_color_manual(values = clust_col, guide = FALSE), 
  labels = "AUTO")
ggsave("./figures/NMDS_clusters.pdf", height = 6, width = 12)

plot_grid(
  ggplot(diversity.df, aes(x = Bottom.depth..m., y = NMDS1, color = cluster))+
    geom_point()+
    scale_color_manual(values = clust_col, guide = FALSE)+ 
    scale_x_log10(),
  
  ggplot(diversity.df, aes(x = Bottom.depth..m., y = NMDS2, color = cluster))+
    geom_point()+
    scale_color_manual(values = clust_col, guide = FALSE)+ 
    scale_x_log10(),
  
  ggplot(diversity.df, aes(x = Bottom.depth..m., y = NMDS3, color = cluster))+
    geom_point()+
    scale_color_manual(values = clust_col, guide = FALSE)+ 
    scale_x_log10(), plotlist = "AUTO")
ggsave("./figures/NMDS_depth.pdf", height = 12, width = 12)

plot3d(diversity.df$NMDS1, diversity.df$NMDS2, diversity.df$NMDS3, col = clust_col[diversity.df$cluster])

catch_effort_wide<- catch_effort %>% 
  spread(key = Scientific.name, value = Catch.weight..kg., fill = 0) %>% 
  ungroup()

catch_effort_wide <- left_join(catch_effort_wide, diversity.df)

cluster_proportional <- catch_effort_wide %>% 
  gather(key = Scientific.name, value = Catch.weight..kg.,ACANTHASTERIDAE:ZOROASTERIDAE) %>% 
  group_by(Survey.Year, Trip.identifier, Set.number) %>% 
  mutate(Prop.catch = Catch.weight..kg./sum(Catch.weight..kg.,na.rm = TRUE)) %>% 
  ungroup() %>% 
  group_by(cluster, Scientific.name) %>% 
  summarise(Prop.catch = mean(Prop.catch))

cluster_proportional_dominant <- cluster_proportional %>% 
  ungroup() %>% 
  group_by(Scientific.name) %>% 
  mutate(cluster.prop.max = max(Prop.catch)) %>% 
  arrange(desc(cluster.prop.max)) %>% 
  filter(cluster.prop.max > 0.1)

cluster_proportional_dominant$Scientific.name <- factor(cluster_proportional_dominant$Scientific.name, levels = unique(cluster_proportional_dominant$Scientific.name[order(cluster_proportional_dominant$cluster.prop.max)]), ordered = TRUE) 

plot_grid(
  ggplot(cluster_proportional_dominant, aes(x = cluster, y = Prop.catch, fill = Scientific.name))+
    #geom_point(data = data.frame(cluster = levels(cluster_proportional_dominant$cluster), Prop.catch = 1), aes(fill = NULL), size = 5, color = clust_col)+
    geom_bar(position="stack", stat="identity", color = "black")+
    scale_fill_viridis_d(option = "B")+
    scale_y_continuous(limits = c(0,1))+
    ylab("Mean proportion of trawl")+
    xlab("Compositional cluster"),
  
  bathy.map+
    geom_point(data = diversity.df, aes(color = factor(cluster)), size = 0.5)+
    scale_color_manual(values = clust_col, name = "Compositional\ncluster")+
    guides(colour = guide_legend(override.aes = list(size=3)))+
    geom_contour(data = bathy.df, aes(z = Bottom.depth..m.), breaks = -exp(seq(log(100),log(3000), length = 15)), size = 0.1, color = "grey80")
  ,
  align = "h", labels = "AUTO"
)
ggsave("./figures/cluster_map.pdf", height = 6, width = 12)

plot_grid(
  ggplot(diversity.df, aes(y = Bottom.depth..m., x = factor(cluster), fill = factor(cluster)))+
    geom_violin()+
    scale_fill_manual(values = clust_col, guide = FALSE)+
    scale_y_reverse()+
    #scale_y_log10()+
    ylab("Depth (m)")+
    xlab("Compositional cluster"),
  
  ggplot(diversity.df, aes(y = `0`, x = factor(cluster), fill = factor(cluster)))+
    geom_violin()+
    scale_fill_manual(values = clust_col, guide = FALSE)+
    ylab("Species richness")+
    xlab("Compositional cluster"),
  
  ggplot(diversity.df, aes(y = `2`, x = factor(cluster), fill = factor(cluster)))+
    geom_violin()+
    scale_fill_manual(values = clust_col, guide = FALSE)+
    scale_y_log10()+
    ylab("Shannon diversity")+
    xlab("Compositional cluster"),
  align = "h", labels = "AUTO", nrow = 1
)
ggsave("./figures/cluster_attributes.pdf", height = 6, width = 12)


plot_grid(
  ggplot(diversity.df, aes(x = Bottom.depth..m., y = Total.biomass.kg.hr, color = cluster, group = NA))+
    geom_point()+
    scale_color_manual(values = clust_col, guide = FALSE)+ 
    scale_x_log10()+
    scale_y_log10(),
  
  ggplot(diversity.df, aes(x = `0`, y = Total.biomass.kg.hr, color = cluster, group = NA))+
    geom_point()+
    scale_color_manual(values = clust_col, guide = FALSE)+ 
    scale_y_log10(),
  
  ggplot(diversity.df, aes(x = `1`, y = Total.biomass.kg.hr, color = cluster, group = NA))+
    geom_point()+
    scale_color_manual(values = clust_col, guide = FALSE)+ 
    scale_y_log10()+
    scale_x_log10(),
  
  ggplot(diversity.df, aes(x = `2`, y = Total.biomass.kg.hr, color = cluster, group = NA))+
    geom_point()+
    scale_color_manual(values = clust_col, guide = FALSE)+ 
    scale_y_log10()+
    scale_x_log10(), 
  nrow = 2, labels = "AUTO")
ggsave("./figures/Total.biomass.pdf", height = 12, width = 12)

summary(lm(data = diversity.df, log(Total.biomass.kg.hr) ~ log(`0`)))
summary(lm(data = diversity.df, log(Total.biomass.kg.hr) ~ log(`1`)))
summary(lm(data = diversity.df, log(Total.biomass.kg.hr) ~ log(`2`)))
